fn main() {
    let text = std::fs::read_to_string("syntax").unwrap();
    let tokens = target::lexer::lex(text);
    let node = target::parser::parse(tokens, &target::BracketType::OpenSquareBracket);
    target::runtime::run(node);
}
