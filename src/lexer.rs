use crate::{Token, BracketType};

fn gen_token(text: &str) -> Token {
    if text.chars().all(|x| x.is_numeric()) && text.contains('.') {
        Token::Float(text.parse().unwrap())
    } else if text.chars().all(|x| x.is_numeric()) {
        Token::Integer(text.parse().unwrap())
    } else if text.starts_with('"') && text.ends_with('"') {
        Token::String(text[1..text.len()-1].to_string())
    } else if text == "{" {
        Token::Bracket(BracketType::OpenCurlyBracket)
    } else if text == "}" {
        Token::Bracket(BracketType::CloseCurlyBracket)
    } else if text == "[" {
        Token::Bracket(BracketType::OpenSquareBracket)
    } else if text == "]" {
        Token::Bracket(BracketType::CloseSquareBracket)
    } else {
        Token::Word(text.to_string())
    }
}

pub fn lex(text: String) -> Vec<Token> {

    let mut result = vec![];
    let mut builder = String::new();
    let mut in_comment = false;
    let mut in_string = false;

    for chr in text.chars() {
        if in_comment && chr != '\n' {
            continue;
        } else if chr == '#' {
            in_comment = true;
        } else if chr == '"' {
            builder.push(chr);
            in_string = !in_string;
        } else if chr.is_whitespace() {
            if !builder.is_empty() && !in_string {
                let token = gen_token(&builder);
                result.push(token);
                builder.clear();
            }
        } else {
            builder.push(chr);
        }
        if chr == '\n' {
            in_comment = false;
        }
    }

    result
}
