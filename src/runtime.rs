use std::collections::HashMap;
use crate::{Node, Value};

fn eval_node(node: Node) -> Value {
    match node {
        Node::Block(inner) => Value::Block(inner),
        Node::FloatLiteral(num) => Value::Float(num),
        Node::IntLiteral(num) => Value::Integer(num),
        Node::StringLiteral(text) => Value::String(text),
        Node::ArrayLiteral(inner) => Value::Array(inner
            .iter()
            .map(|x| eval_node(x.clone()))
            .collect()),
        _ => panic!("Node not evaluable")
    }
}

fn execute_word(word: &str, stack: &mut Vec<Value>, vars: &mut HashMap<String, Value>) {
    match word {
        "print" => {
            let top = stack.pop().unwrap();
            println!("{:?}", top);
        },
        "dup" => {
            let top = stack.pop().unwrap();
            stack.push(top.clone());
            stack.push(top);
        },
        "set" => {
            let name = stack.pop().unwrap();
            let value = stack.pop().unwrap();
            if let Value::String(name) = name {
                vars.insert(name, value);
            } else {
                panic!("'Set' must have string key")
            }
        },
        "map" => {
            let func = stack.pop().unwrap();
            let array = stack.pop().unwrap();
            if let (Value::Array(array), Value::Block(func)) = (array, func) {
                let mut result = vec![];
                for item in array {
                    let mut new_stack = vec![item];
                    for node in func.clone() {
                        execute_node(node, &mut new_stack, vars);
                    }
                    result.append(&mut new_stack)
                }
                stack.push(Value::Array(result));
            }
        },
        "*" => {
            let first = stack.pop().unwrap();
            let second = stack.pop().unwrap();
            stack.push(first*second);
        }
        _ => {
            if let Some(Value::Block(func)) = vars.get(word) {
                for node in func.clone() {
                    execute_node(node.clone(), stack, vars);
                }
            } else {
                panic!("Unknown word {}", word)
            }
        }
    }
}

fn execute_node(node: Node, stack: &mut Vec<Value>, vars: &mut HashMap<String, Value>) {
    if let Node::Word(word) = node {
        execute_word(&word, stack, vars);
    } else {
        stack.push(eval_node(node));
    }
}

pub fn run(root: Node) {

    let mut stack = vec![];
    let mut vars = HashMap::new();

    if let Node::Block(nodes) = root {
        for node in nodes {
            execute_node(node, &mut stack, &mut vars);
        }
    } else {
        panic!("Wrong root")
    }
}
