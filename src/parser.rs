use common_macros::hash_map;
use crate::{Token, Node, BracketType};

fn gen_node(token: Token) -> Node {
    match token {
        Token::String(text) => Node::StringLiteral(text),
        Token::Integer(num) => Node::IntLiteral(num),
        Token::Float(num) => Node::FloatLiteral(num),
        Token::Word(text) => Node::Word(text),
        Token::Bracket(_) => panic!("Unexpected bracket in gen_token")
    }
}

pub fn parse(tokens: Vec<Token>, bracket_type: &BracketType) -> Node {

    let bracket_map = hash_map!(
        BracketType::OpenSquareBracket => BracketType::CloseSquareBracket,
        BracketType::OpenCurlyBracket => BracketType::CloseCurlyBracket
    );

    let mut result = vec![];
    let mut builder = vec![];
    let mut bracket_level = 0;
    let mut outer_bracket = None;

    for token in tokens {
        if let Some(target) = &outer_bracket {
            if let Token::Bracket(bracket) = &token {
                if bracket == target {
                    bracket_level += 1;
                    builder.push(token);
                } else if bracket == bracket_map.get(target).unwrap() {
                    bracket_level -= 1;
                    if bracket_level == 0 {
                        let inner = parse(builder.clone(), target);
                        result.push(inner);
                        builder.clear();
                        outer_bracket = None;
                    } else {
                        builder.push(token);
                    }
                } else {
                    builder.push(token);
                }
            } else {
                builder.push(token);
            }
        } else {
            if let Token::Bracket(bracket) = token {
                if bracket_map.contains_key(&bracket) {
                    outer_bracket = Some(bracket);
                    bracket_level += 1;
                } else {
                    panic!("Closing bracket with no open")
                }
            } else {
                result.push(gen_node(token));
            }
        }
    }

    match bracket_type {
        BracketType::OpenSquareBracket => Node::Block(result),
        BracketType::OpenCurlyBracket => Node::ArrayLiteral(result),
        _ => panic!("Invalid bracet type in parser")
    }
}
