pub mod lexer;
pub mod parser;
pub mod runtime;

#[derive(Debug, Clone)]
pub enum Token {
    String(String),
    Integer(i128),
    Float(f64),
    Word(String),
    Bracket(BracketType)
}

#[derive(Debug, Eq, PartialEq, Hash, Clone)]
pub enum BracketType {
    OpenCurlyBracket,
    CloseCurlyBracket,
    OpenSquareBracket,
    CloseSquareBracket
}

#[derive(Debug, Clone)]
pub enum Node {
    Block(Vec<Node>),
    Word(String),
    IntLiteral(i128),
    FloatLiteral(f64),
    StringLiteral(String),
    ArrayLiteral(Vec<Node>)
}

#[derive(Debug, Clone)]
pub enum Value {
    Block(Vec<Node>),
    Array(Vec<Value>),
    Integer(i128),
    Float(f64),
    String(String)
}

impl std::ops::Mul for Value {
    type Output = Value;
    fn mul(self, other: Value) -> Value {
        match (self, other) {
            (Value::Integer(num1), Value::Integer(num2)) => Value::Integer(num1*num2),
            (Value::Float(num1), Value::Float(num2)) => Value::Float(num1*num2),
            _ => panic!("Incompatible types for mul")
        }
    }
}
